import * as vscode from "vscode";
import { makeNote } from "./note";
import { linkToClipboard } from "./link";
import { openInBrowser } from "./browser";

export function activate(context: vscode.ExtensionContext) {
  console.log('Congratulations, your extension "mzettel" is now active!');

  const makeNoteCmd = vscode.commands.registerCommand(
    "mzettel.makeNote",
    () => {
      makeNote();
    }
  );
  context.subscriptions.push(makeNoteCmd);

  const linkToClipboardCmd = vscode.commands.registerCommand(
    "mzettel.linkToClipboard",
    () => {
      linkToClipboard();
    }
  );
  context.subscriptions.push(linkToClipboardCmd);

  const openBrowserCmd = vscode.commands.registerCommand(
    "mzettel.openInBrowser",
    () => {
      openInBrowser();
    }
  );
  context.subscriptions.push(openBrowserCmd);
}

// this method is called when your extension is deactivated
export function deactivate() {}
