import * as vscode from "vscode";
import { getNotePath, getEditor } from "./util";

export function openInBrowser() {
    const editor = getEditor();
  
    const rootPath = getNotePath();
    const fileName = editor.document.fileName.substr(rootPath.length + 1); // TODO: Remove getNotePath
  
    const url = "http://127.0.0.1:9766/?stackedNotes=" + fileName; // TODO: Make this configurable via workspace settings. See ./util
    console.log("Opening" + url);
    vscode.env.openExternal(vscode.Uri.parse(url));
  }