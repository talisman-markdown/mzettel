This is a fork of  [Pierre Monferrand's mzettel project][1] intended for personal use. The changes made are

# Mzettel

## Contributions to Iroh
- Mzettel - Note: Asks for a title. Creates a new note with **slugified title + extension** as filename and adds title as content.
- Mzettel - Note Link to Clipboard: Copies markdown link to clipboard in the format `[Title](filename)`
- Mzettel - Open in Browser: Opens current note in browser localhost.

[1]: https://github.com/monferrand/mzettle